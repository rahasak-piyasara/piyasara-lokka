package com.score.groop

import akka.actor.ActorSystem
import com.score.groop.cassandra.CassandraStore
import com.score.groop.config.AppConf
import com.score.groop.elastic.ElasticStore
import com.score.groop.stream.Streamer
import com.score.groop.util.{LoggerFactory, RSAFactory}

object Main extends App with AppConf {

  // actor system mystiko
  implicit val system = ActorSystem.create("mystiko")

  // setup logging
  LoggerFactory.init()

  // set up keys
  RSAFactory.init()

  // create schema/indexes
  CassandraStore.init()
  ElasticStore.init()

  // init streamer
  Streamer.init()

}
