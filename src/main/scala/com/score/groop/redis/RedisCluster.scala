package com.score.groop.redis

import com.score.groop.config.RedisConf
import redis.clients.jedis.{JedisPool, JedisPoolConfig}

trait RedisCluster extends RedisConf {

  lazy val poolConfig = {
    val pool = new JedisPoolConfig()
    pool.setMaxTotal(12)
    pool.setBlockWhenExhausted(true)
    pool
  }
  lazy val redisPool = new JedisPool(poolConfig, redisHost, redisPort)

}
