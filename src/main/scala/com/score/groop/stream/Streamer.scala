package com.score.groop.stream

import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.scaladsl.{Flow, GraphDSL, Partition, RunnableGraph, Sink, Source}
import akka.stream.{ActorMaterializer, ActorMaterializerSettings, ClosedShape, Supervision}
import com.score.groop.cassandra.{Block, CassandraCluster, CassandraStore, Trans}
import com.score.groop.config.{AppConf, KafkaConf}
import com.score.groop.redis.RedisStore
import com.score.groop.util.{AppLogger, BlockFactory}

import scala.collection.mutable
import scala.concurrent.Future
import scala.concurrent.duration._

object Streamer extends AppLogger with CassandraCluster with AppConf with KafkaConf {

  def init()(implicit system: ActorSystem): Unit = {
    // supervision
    // meterializer for streams
    val decider: Supervision.Decider = { e =>
      logError(e)
      Supervision.Resume
    }
    implicit val materializer = ActorMaterializer(ActorMaterializerSettings(system).withSupervisionStrategy(decider))
    import system.dispatcher

    // grooper run for given period
    val source = Source.tick(0.minute, 1.minute, NotUsed)

    // flow to get trans ids from redis
    val getTransIdsFlow = Flow[NotUsed]
      .map(_ => RedisStore.get)

    // flow to get trans from cassandra
    val getTransFlow = Flow[mutable.Set[String]]
      .mapAsync(2)(p => Future(CassandraStore.getTrans(p.toList)))

    // flow to remove redis ids
    val removeTransIdsFlow = Flow[(List[String], List[Option[Trans]])]
      .map { p =>
        RedisStore.rm(p._1)
        p._2
      }

    // flow to build block obj
    val toBlockFlow = Flow[List[Option[Trans]]]
      .map(p => BlockFactory.block(p.flatten))

    // flow to save block
    val saveBlockFlow = Flow[Block]
      .mapAsync(parallelism = 2)(p => CassandraStore.createBlock(p))

    // TODO sink to publish kafka
    val sink = Sink.foreach[Block](p => s"created block $p")

    // graph
    val graph = RunnableGraph.fromGraph(GraphDSL.create() { implicit b =>
      import GraphDSL.Implicits._

      // partition stream
      //  1. with trans
      //  2. without trans
      val partition = b.add(Partition[(List[String], List[Option[Trans]])](2, r => if (r._2.flatten.isEmpty) 0 else 1))

      // source directs to partition
      source ~> getTransIdsFlow ~> getTransFlow ~> partition.in

      // flow stream in two flows
      //  1. with trans
      //  2. without trans
      partition.out(0) ~> Sink.ignore
      partition.out(1) ~> removeTransIdsFlow ~> toBlockFlow ~> saveBlockFlow ~> sink

      ClosedShape
    })
    graph.run()
  }

}
